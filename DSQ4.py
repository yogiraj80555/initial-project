year = [1970,1971,1972,1973,1974,1975]
sales = [3.1,3.3,3.6,3.7,3.2,3.9]
midel = 0
x = []
n = len(year)
if n%2 == 0:
	midel = (year[int(len(year)/2)-1] + year[int((len(year)/2))])/2
else:
	midel = year[int(0.5+(len(year)/2))-1]
x[:] = [x-midel for x in year]
import numpy
Xsquare = numpy.array(x)**2

XandY = [round(sales[i]*x[i],2) for i in range(len(year))]

A = round(sum(sales)/n,4)
B = round(sum(XandY)/sum(Xsquare),4)

print("Estimate the Profit For 1976 is: ")
Y = A+B*(1976-midel)
print(Y)
