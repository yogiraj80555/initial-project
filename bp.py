import numpy as np

# X = (hours sleeping, hours studying), y = score on test
X = np.array(([0, 0.166667], [0.34, 0.84], [1, 1]), dtype=float)
y = np.array(([0.01], [0.86], [0.89]), dtype=float)


class BPNet(object):
  def __init__(self):
    
    self.inputSize = 2
    self.outputSize = 1
    self.hiddenSize = 3


    self.W1 = np.random.randn(self.inputSize, self.hiddenSize) 
    self.W2 = np.random.randn(self.hiddenSize, self.outputSize) 

  def forward(self, X):
    
    self.z = np.dot(X, self.W1)#product of wi and Xi 
    self.z2 = self.sigmoid(self.z) 
    self.z3 = np.dot(self.z2, self.W2) 
    o = self.sigmoid(self.z3) #
    return o 

  def sigmoid(self, s):
    return 1/(1+np.exp(-s))

  def sigmoidDerivative(self, s):
    return s * (1 - s)

  def backward(self, X, y, o):
   
    self.o_error = y - o # error finding
    self.o_delta = self.o_error*self.sigmoidDerivative(o) # finding derivation

    self.z2_error = self.o_delta.dot(self.W2.T) 
    self.z2_delta = self.z2_error*self.sigmoidDerivative(self.z2) 

    self.W1 += X.T.dot(self.z2_delta) 
    self.W2 += self.z2.T.dot(self.o_delta) 

  def next (self, X, y):
    o = self.forward(X)
    self.backward(X, y, o)

backP = BPNet()
for i in range(100): 
  print ("Input: \n" + str(X) )
  print ("Actual Output: \n" + str(y) )
  print ("Predicted Output: \n" + str(backP.forward(X)) )
  print ("Loss: \n" + str(np.mean(np.square(y - backP.forward(X))))) # mean sum squared loss
  print ("\n")
  backP.next(X, y)