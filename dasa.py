from datetime import datetime ,timedelta
import time
from collections import namedtuple
import pandas as pd
import matplotlib.pyplot as pyplot

target_date = datetime(2016, 5, 16)
features = ["date","temperature","apparent_temperature","humidity","wind_speed","wind_bearing","visibility","pressure"]
DailySummary = namedtuple("DailySummary",features)



def get_all_data():
	df = pd.read_csv("Dataset/weatherHistory.csv",usecols =["Formatted Date","Temperature (C)","Apparent Temperature (C)"
	,"Humidity","Wind Speed (km/h)","Wind Bearing (degrees)","Visibility (km)","Loud Cover","Pressure (millibars)"])#index_col ="Formatted Date"
	print(type(df.get("Humidity")))

get_all_data();