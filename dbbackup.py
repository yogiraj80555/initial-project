import os
import time
import datetime

DB_HOST = "127.0.0.1"
DB_USER = "root"
DB_PASS = ""
DB_NAME = "3dface"
BACKUPPATH = "backupfolder/database/"

DATETIME = time.strftime('%m%d%y - %H%M%S')
TODAYBACKUPPATH = BACKUPPATH + DATETIME

print("Creating Folder")
if not os.path.exists(TODAYBACKUPPATH):
	os.makedirs(TODAYBACKUPPATH)

print("Checking Database File")
if os.path.exists(DB_NAME):
	file1 = open(DB_NAME)
	multi = 1
	print("DB FOUND\n Starting Backup")

else:
	print ("DB Not Found")
	multi = 0


if multi:
	in_file = open(DB_NAME,"r")
	flength = len(in_file.readline())
	in_file.close()
	p = 1
	dbfile = open(DB_NAME,"r")

	while p <= flength:
		db = dbfile.readline()
		db = db[:-1]
		dumpcmd = "mysqldump -u"+DB_USER+" -p "+DB_PASS+" "+db+" > "+TODAYBACKUPPATH + "/" + db +" .sql"
		os.system(dumpcmd)
		p = p+1
	dbfile.close()

else: 
	db = DB_NAME
	dumpcmd = "mysqldump -u"+DB_USER+" -p "+DB_PASS+" "+db+" > "+TODAYBACKUPPATH + "/" + db +" .sql"
	os.system(dumpcmd)

print("Backup Script Completed")
print("Your Backups Has Been Ceated in '"+TODAYBACKUPPATH+"' Directory")
		