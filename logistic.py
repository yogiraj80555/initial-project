import math
print("\n\n\t\t\t\tLogistic Regression")
print("\t\t\tUsing Binary Sigmoid Function and Stocastic Gradient")
#n = int(input("\n Enter Number of Inputes: "));
x1 = [];
x2 = [];
y = [];
b0 = 0;b1 = 0;b2 = 0;
alpha = 0.3;
Yhat = 1/(1+math.exp(-0));
x0 = 1
'''
for i in range(n):
	print("\nEnter Record ",i+1);
	x = float(input("\n\nEnter the input X1 "))
	x1.append(x);
	x = float(input("Enter the input X2 "))
	x2.append(x);
	x = float(input("Enter the Output Y "))
	y.append(x);
'''
x1 = [2.78,1.46,3.39,1.38,3.06,7.62,5.63,6.92,8.67,7.67];
x2 = [2.55,2.36,4.40,1.85,3.00,2.75,1.08,1.77,0.24,3.50];
y = [0,0,0,0,0,1,1,1,1,1]
print(len(x1))

print("\nInitial Bias b,b1,b2 is: 0\nLearning Rate is 0.2")
epoch=10000
#epoch = int(input("Enter the Number of Epoch: "));

for i in range(epoch):
	print("Epoch ",i+1,"Started")
	for j in range(len(x1)):
		b0 = round(b0+alpha*(y[j]-Yhat)*Yhat*(1-Yhat)*x0,8)
		b1 = round(b1+alpha*(y[j]-Yhat)*Yhat*(1-Yhat)*x1[j],8)
		b2 = round(b2+alpha*(y[j]-Yhat)*Yhat*(1-Yhat)*x2[j],8)
		#Yhat = 1/(1+math.exp(-(b0+(b1*x1[j])+(b2*x2[j]))))
	print("{} {} {}".format(b0,b1,b2))


print("\n\nFor accuracy");
ac = 0;
for j in range(len(x1)):
	ans = 1/(1+math.exp(-(b0+(b1*x1[j])+(b2*x2[j]))))
	print(ans)
	print("\n")
	if ans < 0.5:
		ans = 0;
	else: ans = 1
	if ans == y[j]:
		ac +=1
print("Accuracy is: ",ac/10);