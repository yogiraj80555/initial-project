import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews
from collections import defaultdict
import numpy as np

SPLIT = 0.8

def word_feats(words):
	feats = defaultdict(lambda:False)
	for word in words:
		feats[word] = True
	return feats

posids = movie_reviews.fileids('pos')
negids = movie_reviews.fileids('neg')

print(posids)