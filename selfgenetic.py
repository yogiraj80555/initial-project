popu_size=10000;
name = "Soft Computing";
import random;
genes = '''abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP 
QRSTUVWXYZ 1234567890, .-;:_!"#%&/()=?@${[]}''';

class Individual(object):

	def __init__(self,chro):
		self.chromosome = chro;
		self.fitness = self.get_fitness();

	@classmethod
	def muted_genes(self):
		global genes;
		return random.choice(genes);

	@classmethod
	def start_gnome(self):
		global name;
		return [self.muted_genes() for x in range(len(name))]




	def get_fitness(self):
		global name;
		fitness = 0;
		for chro, nme in zip(self.chromosome,name):
			if chro != nme:
				fitness += 1;
		return fitness;


	def new_g(self, parent):
		child_chrosomo = [];
		for chrom,parentchrosom in zip(self.chromosome, parent.chromosome):
			prob = random.random();
			#print(prob)
			#print("chrom {},  parent {}  ".format(chrom,parentchrosom))
			if prob <= 0.40:
				child_chrosomo.append(chrom)
			elif prob <= 0.80:
				child_chrosomo.append(parentchrosom)
			else:
				child_chrosomo.append(self.muted_genes())

		return Individual(child_chrosomo)






def main():
	import time
	global popu_size

	generation = 1;
	meet = False;
	population = [];


	for x in range(popu_size):
		gnome = Individual.start_gnome();
		#print(gnome)
		population.append(Individual(gnome));
	#print("\n\n")
	#print(population[0].chromosome)

	while not meet:
		population = sorted(population, key = lambda x:x.fitness) 
		if (population[0].fitness <= 0) :
			meet = True;
			break;

		next_genertion = [];

		next_genertion.extend(population[:int((10*popu_size)/100)]);

		for i in range(int((10*popu_size)/100)):
			parent1 = random.choice(population[:50]);
			parent2 = random.choice(population[:50]);
			child = parent1.new_g(parent2);
			#print(child.chromosome)
			next_genertion.append(child);

		#print(population[8].chromosome)
		#print("\n\n")
		population = next_genertion;
		#print(population[8].chromosome)

		print("Current Generation is: {} \t String is: {} \t It's Fitness is: {}".format(generation,"".join(population[0].chromosome),population[0].fitness))
		generation += 1
		time.sleep(1);
		#meet = True;
	print("Last Generation is: {} \t String is: {}\t It's Fitness is {}".format(generation,"".join(population[0].chromosome),population[0].fitness))

if __name__ == '__main__':
	main();