#pip install tweepy
#pip install TextBlob

import re 
import tweepy 
from tweepy import OAuthHandler 
from textblob import TextBlob 

class TwitterClient(object): 
	
	def __init__(self): 
		
		consumer_key = 'csTnKtdpSgwAnKmn2xsjFO0r7'
		consumer_secret = 'NIfTWik5589d9u7oT9ork2v5bEhepuR0ta7YFYn9zl48WnHMXS'
		access_token = '869111815875067906-PtD71RH4h5OHqV2rVjQzjwg26ygg0hB'
		access_token_secret = 'o6LQZCXqHiXX7UM7yFP7dbGkaABtkmQh0RzhDAVKer2cG'

		
		try: 
			
			self.auth = OAuthHandler(consumer_key, consumer_secret) 
			
			self.auth.set_access_token(access_token, access_token_secret) 
			
			self.api = tweepy.API(self.auth) 
		except: 
			print("Error: Authentication Failed") 

	def clean_tweet(self, tweet): 
		
		return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split()) 

	def get_tweet_sentiment(self, tweet): 
		
		
		analysis = TextBlob(self.clean_tweet(tweet)) 
		
		if analysis.sentiment.polarity > 0: 
			return 'positive'
		elif analysis.sentiment.polarity == 0: 
			return 'neutral'
		else: 
			return 'negative'

	def get_tweets(self, query, count = 10): 
		
		
		
		
		tweets = [] 

		try: 
			
			fetched_tweets = self.api.search(q = query, count = count) 

			
			for tweet in fetched_tweets: 
				
				parsed_tweet = {} 

				
				parsed_tweet['text'] = tweet.text 
				
				parsed_tweet['sentiment'] = self.get_tweet_sentiment(tweet.text) 

			
				if tweet.retweet_count > 0: 
			
					if parsed_tweet not in tweets: 
						tweets.append(parsed_tweet) 
				else: 
					tweets.append(parsed_tweet) 

			
			return tweets 

		except tweepy.TweepError as e: 
			# print error (if any) 
			print("Error : " + str(e)) 

def write_to_file(data):
	file = open('sentiment.csv','w+')
	file.write("Tweets , Sentiment\n");
	for text in data:
		file.write(str(text[0].encode("utf-8"))+","+text[1]+"\n")
	file.close

def replace_comma(data):
	return data.replace(","," ")




def main(): 
	
	api = TwitterClient() 

	tweets = api.get_tweets(query = 'Election', count = 200) 

	#print(len(tweets))
	 
	ptweets = [tweet for tweet in tweets if tweet['sentiment'] == 'positive'] 
	
	print("Positive tweets percentage: {} %".format(100*len(ptweets)/len(tweets))) 
	
	ntweets = [tweet for tweet in tweets if tweet['sentiment'] == 'negative'] 

	nuttweets = [tweet for tweet in tweets if tweet['sentiment'] == 'neutral'] 

	allData = [];

	
	print("Negative tweets percentage: {} %".format(100*len(ntweets)/len(tweets))) 
	
	print("Neutral tweets percentage: {} % \n".format(100*(len(tweets) - len(ntweets) - len(ptweets))/len(tweets))) 

	
	#print("\n\nPositive tweets:") 
	for tweet in ptweets: 
		#print(tweet['text']) 

		allData.append([replace_comma(tweet['text']),tweet['sentiment']])

	# printing first 5 negative tweets 
	#print("\n\nNegative tweets:") 
	for tweet in ntweets: 
		#print(tweet['text']) 
		#print("\n")
		allData.append([replace_comma(tweet['text']),tweet['sentiment']])


	#print("\n\nNeutral Tweets:")
	for tweet in nuttweets:
		#print(tweet['text'])
		#print("\n")
		allData.append([replace_comma(tweet['text']),tweet['sentiment']])

	#print(str(allData))
	write_to_file(allData)


if __name__ == "__main__": 
	main()


