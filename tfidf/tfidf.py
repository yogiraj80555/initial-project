from nltk.stem import PorterStemmer
import math

def openFileReadData(file,index):
	filename = file + str(index)
	file = open(filename,'r')
	filedata = ""
	if file.mode == 'r':
		filedata = file.read()
	file.close
	return filedata

def writeToFile(file,index,dataList,id = 0):
	filename = file + str(index)
	file = open(filename, 'w+')
	if id == 1:
		file.write(str(dataList))
		return file.close
	for i in range(0,len(dataList)):
		file.write(dataList[i]+" ")
	file.close
	return len(dataList)

def getStopword():
	return openFileReadData("stopwords",0)

def getSymbols():
	return openFileReadData("symbols",0)

def removeStopwords(textList,stopwordList):
	data = []
	for word in textList:
		if word not in stopwordList:
			data.append(word)	
	return data

def removeSymbols(textData,symbolsList):
	text = textData
	for i in symbolsList:
		text = text.replace(i,"")
	return text

def convertLower(file):
	return file.lower()

def wordSteaming(dataList):
	steamWord = []
	for i in dataList:
		steamWord.append(PorterStemmer().stem(i))
	return steamWord

def findUniqueKeys(unique,data):
	for word in data:
		if word not in unique:
			unique.append(word)
	return unique
################################################################################################################################################################
def tfDiretory(keys,count,datas = ""):
	if count == 0:
		data = dict()
		for i in keys:
			data[i] = 0
		return data
	if count == 1:
		for i in keys:
			if i in datas:
				datas[i] +=1
		return datas 

def terFreq(fileDict,unique_keys,totvalues):
	for i in unique_keys:
		count = fileDict[i]
		if count != 0:
			fileDict[i] = count/totvalues
	return fileDict

def idfDirectory(idfData,keys,filedata):
	dicts = {}
	for i in keys:
		if i in filedata:
			dicts[i] = 1
		else:
			dicts[i] = 0
	return dicts

def tfIdfInfo(tfidfInfo,keys,data):
	for i in keys:
		if tfidfInfo[i] == 1:
			data[i] +=1
	return data
def calculatingIdf(data,keys):
	for i in keys:
		nbr = data[i]
		data[i] = math.log(no_of_file/nbr)
	return data

################################################################################################################################################################
## Real Code
#################################################################################################################################################################

stopword = getStopword().split()
symbols = getSymbols().split()
unique_keys = []
no_of_file = 0
for i in range(1,4):
	no_of_file += 1
	fileData = openFileReadData('txt',i)
	data = convertLower(fileData)
	data = removeSymbols(data,symbols).split()
	data = removeStopwords(data,stopword)
	nbr = writeToFile("token" ,i,data)
	data = wordSteaming(data)
	nbr = writeToFile("steam",i,data)
	################################################################################################################################################################
	## for finding TF-IDF we have to find all distinct key word in all files
	################################################################################################################################################################
	unique_keys = findUniqueKeys(unique_keys,data)
	nbr = writeToFile('alluniquewords',0,unique_keys)
	################################################################################################################################################################
	## uptill all text are formatted as per required
	################################################################################################################################################################


####################################################################################################################################################################
## Calculating TF IDF and make dictionary with 0 count
####################################################################################################################################################################
ownDict = tfDiretory(unique_keys,0)
idfDict = tfDiretory(unique_keys,0)
idfData = {}
idfDataFilesInfo = []
for i in range(1,4):
	fileData = openFileReadData('steam',i).split()
	ownDict = tfDiretory(fileData,1,ownDict)
	###########################################################################################################################################################
	## single file term Frequency
	###########################################################################################################################################################
	tfDict = tfDiretory(unique_keys,0)
	fileDict = tfDiretory(fileData,1,tfDict)
	termFrq = terFreq(fileDict,unique_keys,len(fileData))
	writeToFile("tfFile",i,termFrq,1)
	###########################################################################################################################################################
	###########################################
	## for IDF
	###########################################
	idfData = idfDirectory(idfData,unique_keys,fileData)
	writeToFile("idfFile",i,idfData,1)
	idfDataFilesInfo.append(idfData)
	writeToFile("totalIdf",0,idfDataFilesInfo,1)
	idfDict = tfIdfInfo(idfDataFilesInfo[i-1],unique_keys,idfDict)
	writeToFile("idf",0,idfDict,1)
#################################################################################
idf = calculatingIdf(idfDict,unique_keys)
print(idf)
writeToFile("idf0",0,idf,1)



