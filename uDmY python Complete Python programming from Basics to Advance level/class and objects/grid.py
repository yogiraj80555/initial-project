from tkinter import *
from tkinter import ttk

root = Tk()
label1 = Label(root, text="Label 1", bg="green")
label1.grid(row=0,column=0,ipadx=50,ipady=50); #here padx is an width and pady is an height

label2 = Label(root, text="Label 2", bg="Yellow")			
label2.grid(row=0,column=1,sticky=EW+NS)

label3 = Label(root,text="Label 3",bg="pink")
label3.grid(row=1,column=0,sticky=N+W+E)

label4 = Label(root,text="Label 4",bg="blue")
label4.grid(column=1,row=1,ipadx=50,ipady=50)

"""	Sticky			 _______________________
   N			|NW	  N	  NE|			|
W__|__E			|	  |		|			|
   |			|W ___|___ E|			|
   S 			|	  |		|			|
				|     |		|			|
				|SW	  S   SE|			|
				|___________|___________|	
    			|			|			|
    			|			|			|
    			|			|			|
				|			|			|
				|			|			|
				|			|			|
				|___________|___________|
"""



root.mainloop()
