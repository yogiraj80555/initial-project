from tkinter import *
from tkinter import ttk

def openclk():
	print("Open was Clicked")

def quitapp():
	quit();

root = Tk()
menu = Menu(root)
root.config(menu = menu)
subMenu = Menu(menu,tearoff=0)
menu.add_cascade(label = 'File', menu=subMenu)
subMenu.add_command(label="open", command=openclk)
subMenu.add_separator()
subMenu.add_command(label="Save")
subMenu.add_command(label="Save as")
subMenu.add_separator()
subMenu.add_command(label="Quit", command=quitapp)


subMenu2 = Menu(menu,tearoff=0)
menu.add_cascade(label = 'Edit' , menu = subMenu2)
subMenu2.add_command(label="Delete")
subMenu2.add_separator()
subMenu2.add_command(label="Copy")
subMenu2.add_command(label="Past")
subMenu2.add_separator()
subMenu2.add_command(label="Undo")
root.mainloop()