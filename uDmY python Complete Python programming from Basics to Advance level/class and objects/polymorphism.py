class Shape:
	width = 0;
	height = 0;

	def area(self):
		print("Parent Class Area");

	def swap(self):
		print("Before Swaping {}   {}".format(self.width,self.height))
		self.width = self.width + self.height
		self.height = self.width - self.height
		self.width = self.width - self.height
		print("After Swaping {}   {}".format(self.width,self.height))

class Rectangle(Shape):

	def __init__(self,w,h):
		self.width = w;
		self.height = h;

	def area(self):
		print("Area of Rectangle is: ",self.width * self.height)

class Triangle(Shape):
	def __init__(self,w,h):
		self.width = w;
		self.height = h;

	def area(self):
		print("Area of Triangle is: ",(self.width * self.height)/2)




class Sides(Triangle,Rectangle):
	def __init__(self,w,h):
		self.width = w;
		self.height = h;

	def area(self):
		print("New Class Sides");


ractangle = Rectangle(10,20);
triangle = Triangle(-2,10);
sides = Sides(-20,-30);
triangle.area();
ractangle.area();
triangle.swap();
sides.area();
sides.swap();
