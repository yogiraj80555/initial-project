mylst = [1,2,3,[2,[1,2,6,"string"],"hamza"],4,85,89]
print(mylst[3][2])
print(mylst[3][0])
print(mylst[3][1][3])
print(mylst[3][1][:3])
print(mylst[3][1][3][:3])#printing t in string

"""
Method on List
extend
append
insert
remove
clear
count
sort
reverse
copy
pop(thus we can use list as stack)
index

"""
mylst.extend(mylst)
print(mylst)
mylst.extend('Mango')#see also
print(mylst)