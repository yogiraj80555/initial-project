import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

training_data = pd.read_csv(r'Dataset/aapllmt.csv')

training_processed = training_data.iloc[:,1:2].values

from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler(feature_range = (0,1))

training_scaled = scaler.fit_transform(training_processed);

#print(training_scaled)

features_set = []
labels = []

for i in range(60,1260):
	features_set.append(training_scaled[i-60:i,0])
	labels.append(training_scaled[i,0])


features_set, labels = np.array(features_set),np.array(labels)
#print(labels)

features_set = np.reshape(features_set,(features_set.shape[0],features_set.shape[1],1))
print(features_set)
 

#from keras.models import Sequential



#18.5204   73.8567