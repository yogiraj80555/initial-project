import os
import time
from collections import namedtuple
from datetime import datetime, timedelta
import pandas as pd
import requests
from pyprind import ProgBar

API_KEY = os.environ.get('MY_API_KEY')
BASE_URL = 'http://api.wunderground.com/api/{}/history_{}/q/TX/Round_Rock.json'

features = [
    "date", "meantempm", "meandewptm", "meanpressurem", "maxhumidity",
    "minhumidity", "maxtempm", "mintempm", "maxdewptm", "mindewptm",
    "maxpressurem", "minpressurem", "precipm"
]
target_date = datetime(2017, 9, 27)  
DailySummary = namedtuple('DailySummary', features)

def extract_data(dataset="Dataset.csv"):
    records = []
    df = pd.read_csv(dataset,header=[0]);
    bar = ProgBar(len(df))
    import sys
    print("Loading please wait ......")
    for i in range(len(df)):
        records.append(DailySummary(
            date=df['date'][i],
            meantempm=df['meantempm'][i],
            meandewptm=df['meandewptm'][i],
            meanpressurem=df['meanpressurem'][i],
            maxhumidity=df['maxhumidity'][i],
            minhumidity=df['minhumidity'][i],
            maxtempm=df['maxtempm'][i],
            mintempm=df['mintempm'][i],
            maxdewptm=df['maxdewptm'][i],
            mindewptm=df['mindewptm'][i],
            maxpressurem=df['maxpressurem'][i],
            minpressurem=df['minpressurem'][i],
            precipm=df['precipm'][i]))
        #time.sleep(0.05)
        sys.stdout.write("-")
        sys.stdout.flush()
        bar.update()
    print("\nDataset Loaded successfully")
    time.sleep(4)
    return records

def create_nth_day_feature(df,feature,N=1):
    rows = df.shape[0];
    #print(rows);
    nth_prior_measurement = [None]*N+[df[feature][i-N] for i in range(N,rows)];
    #print(rows)
    col_name = "{}_{}".format(feature,N)
    df[col_name] = nth_prior_measurement


def data(df,feature,N=1):
    for i in range(N,df.shape[0]):
        print(df[feature][i-N])


records = extract_data()
import pandas as pd
df = pd.DataFrame(records, columns=features).set_index('date')
print(df.head())
#tmp = df[['meantempm','meandewptm']].head(10)
#print(tmp)
#N = 1;
#feature = 'meantempm';

for feature in features:
    if feature != "date":
        for N in range(1,4):
            create_nth_day_feature(df,feature,N)
            #data(df,feature,N)
#print(df.head())
#print(df.columns)

#removing unwanted data
to_remove = [feature for feature in features
                        if feature not in ['meantempm','mintempm','maxtempm']]
#print(to_remove)
to_keep = [data for data in df.columns 
                    if data not in to_remove]
#print(to_keep)
df = df[to_keep]
#print(df.columns)
#df.info()

#convert object to float and int
df = df.apply(pd.to_numeric,errors='coerce')
#df.info()
#print(df.columns)

#print(df[['mintempm_1','mintempm_2','maxdewptm_1','maxdewptm_2','maxdewptm_3']].describe().T)

spread = df.describe().T
#print(spread)

IQR = spread['75%'] - spread['25%']
#print(IQR)
spread['outliers'] = (spread['min']<(spread['25%']-(3*IQR)))|(spread['max'] > (spread['75%']+3*IQR))
#print(spread['outliers'])
spread.loc[spread.outliers]
#print(spread.loc[spread.outliers][['min','25%','50%','75%','max','mean','std']])

import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [14,8]
df.maxhumidity_1.hist()
plt.title("Distribution of Humidity")
plt.xlabel("Max Humidity")
plt.show()

df.minpressurem_1.hist()
plt.title("Distribution of Pressure")
plt.xlabel("Min Pressure")
plt.show()


#removing NaN ie None
df = df.fillna(0)
'''
for prec in ['precipm_1','precipm_2','precipm_3']:
    missing_idx = pd.isnull(df[prec])
    df[prec][missing_idx] = 0
'''
print(df.head())
#print(df.columns)



##next part
#pearsons corelation
#print(df.corr()[['meantempm']].sort_values('meantempm'))
df.corr()[['meantempm']].sort_values('meantempm')


predictors = ['meantempm_1',  'meantempm_2',  'meantempm_3',  
              'mintempm_1',   'mintempm_2',   'mintempm_3',
              'meandewptm_1', 'meandewptm_2', 'meandewptm_3',
              'maxdewptm_1',  'maxdewptm_2',  'maxdewptm_3',
              'mindewptm_1',  'mindewptm_2',  'mindewptm_3',
              'maxtempm_1',   'maxtempm_2',   'maxtempm_3']

df2 = df[['meantempm']+predictors]

import numpy as np

plt.rcParams['figure.figsize'] = [16,22]

fig, axes = plt.subplots(nrows=6, ncols=3, sharey=True)


arr = np.array(predictors).reshape(6,3)

for row, col_arr in enumerate(arr):
	for col, feature in enumerate(col_arr):
		axes[row, col].scatter(df2[feature],df2['meantempm'])
		if col == 0:
			axes[row,col].set(xlabel=feature,ylabel='meantempm')
		else:
			axes[row,col].set(xlabel=feature)
plt.show()



import statsmodels.api as sm

X = df2[predictors]
y = df2['meantempm']

X = sm.add_constant(X)
#print(X.head())


alpha = 0.05

model = sm.OLS(y,X).fit()



X = X.drop('meandewptm_3',axis=1)


model = sm.OLS(y,X).fit()

print(model.summary())

from sklearn.model_selection import train_test_split  
X = X.drop('const', axis=1)
x_train, x_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=10)

from sklearn.linear_model import LinearRegression

regressor = LinearRegression()

regressor.fit(x_train,y_train)

prediction = regressor.predict(x_test)

from sklearn.metrics import mean_absolute_error,median_absolute_error
print("The Variance is: %.4f"% regressor.score(x_test,y_test))
print("The Absolute Mean Error: %.4f Celsius"% mean_absolute_error(y_test,prediction))
print("The Absolute Median Error:  %.4f Celsius"% median_absolute_error(y_test,prediction))